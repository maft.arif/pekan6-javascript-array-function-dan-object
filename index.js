// ------ SOAL 1 ------
 
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
var daftarHewanUrut = daftarHewan.sort().slice()
var text=""
for (let i = 0; i < daftarHewanUrut.length; i++) { 
    text += daftarHewanUrut[i] + "\n";
}
console.log(text)



// ------ SOAL 2 ------

function introduce(bio) {
    return "Nama saya "+ bio.name+", umur saya "+bio.age+" tahun, alamat saya di "+bio.address+", dan saya punya hobby yaitu "+bio.hobby+"\n"
} 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan)



// ------ SOAL 3 ------

function hitung_huruf_vokal(nama) {
    hurufVokal=0
    for (let i = 0; i < nama.length; i++) {
        if (nama[i].toLowerCase() == "a" || nama[i].toLowerCase() == "i" || nama[i].toLowerCase() == "u" || nama[i].toLowerCase() == "e" || nama[i].toLowerCase() == "o") {
            hurufVokal++
        }
    }
    return hurufVokal
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1, hitung_2)



// ------ SOAL 4 ------

function hitung(angka) {
    return angka * 2 - 2
}
console.log("Jawaban No 4.\n")
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
